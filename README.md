# Charge CLI tool

### Installation

`go get -u gitlab.com/supaflyENJOY/charge`

### Usage
`charge post organizations --authorization=MY_API_KEY --body=./body.json --data=base64`
