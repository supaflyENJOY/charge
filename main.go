package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/urfave/cli/v2"
)

const hostname = "http://charge.petrosyan.in"
const authorizationKey = "authorization"
const bodyPathKey = "body"
const dataTypeKey = "data"

func postOrganizationAction(c *cli.Context) error {
	apiKey := c.String(authorizationKey)
	bodyPath := c.Path(bodyPathKey)
	dataType := c.String(dataTypeKey)

	if len(apiKey) == 0 && len(bodyPath) == 0 {
		fmt.Println("Error:", authorizationKey, "&", bodyPathKey, "flags should be both provided!")
		return nil
	}
	if _, err := os.Stat(bodyPath); os.IsNotExist(err) {
		fmt.Println("Error: body path is not valid!")
		return nil
	}

	f, err := os.Open(bodyPath)
	if err != nil {
		fmt.Println("Error: couldn't open file!")
		return nil
	}
	defer f.Close()

	url := fmt.Sprintf("%s/organizations?data=%s", hostname, dataType)

	req, err := http.NewRequest("POST", url, f)
	if err != nil {
		panic(err)
	}

	apiKeyHeader := fmt.Sprintf("Api-Key %s", apiKey)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", apiKeyHeader)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error: server isn't available!")
		return nil
	}
	defer resp.Body.Close()

	fmt.Println("Response Status:", resp.Status)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	return nil
}

func main() {
	app := &cli.App{
		Name:  "charge",
		Usage: "cli tool provided for fullstack take-home task by charge",
		Commands: []*cli.Command{
			{
				Name:    "post",
				Aliases: []string{"p"},
				Usage:   "make a post request to the server",
				Subcommands: []*cli.Command{
					{
						Name:  "organizations",
						Usage: "post an organization to the server",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:  authorizationKey,
								Value: "",
								Usage: "API `key` used to make request",
							},
							&cli.PathFlag{
								Name:  bodyPathKey,
								Value: "",
								Usage: "`path` to request body",
							},
							&cli.StringFlag{
								Name:  dataTypeKey,
								Value: "base64",
								Usage: "`format` of returned data",
							},
						},
						Action: postOrganizationAction,
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
